package se.bbs.music.services;

import se.bbs.music.models.Artist;

/**
 * Created by gideonoduro on 2016-02-09.
 */
public interface IMusicService {

    public Artist find(String id);
}
