package se.bbs.music.services;

import org.apache.log4j.Logger;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.client.HttpClientErrorException;
import se.bbs.music.*;
import se.bbs.music.models.Artist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se.bbs.music.models.Relations;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Music service uses uses three rest clients to resolve data from separate API:s.
 * Service also implements a simple cache that stores the combined data model Artist,
 * for that application session.
 *
 *@author Gideon Oduro <gideon.oduro@gmail.com>
 * */

@Service
public class MusicService implements IMusicService {

    private static final Logger log = Logger.getLogger(MusicService.class);

    private IMusicbrainzIdClient musicbrainzIdClient;
    private IMediaWikiClient mediaWikiClient;
    private ICoverArtClient coverArtClient;

    @Autowired
    public MusicService(MusicbrainzIdClient musicbrainzIdClient,
                        MediaWikiClient mediaWikiClient,
                        CoverArtClient coverArtClient){
        this.musicbrainzIdClient = musicbrainzIdClient;
        this.mediaWikiClient = mediaWikiClient;
        this.coverArtClient = coverArtClient;
    }

    @Override
    @Cacheable("Artists")
    public Artist find(String id) {
        Artist artist = musicbrainzIdClient.findArtist(id);
        if (artist != null){
            try {
                removeUnUsedRelations(artist);
                String resourceUrl = artist.getRelationsList().get(0).getResourceUrl().getUrl();
                String description = mediaWikiClient.findDescription(resourceUrl);
                responseBuilder(artist, description);
            } catch (HttpClientErrorException ex){
                log.error("Encounter a issue when trying to retrieve data for ID: " + id);
            }

        }
        return artist;
    }

    private Artist removeUnUsedRelations(Artist artist) throws HttpClientErrorException {
        List<Relations> relationsList = artist.getRelationsList();
        List<Relations> filteredList = relationsList
                .stream()
                .filter(relations -> relations.getType().equals("wikipedia"))
                .collect(Collectors.toList());
        artist.setRelationsList(filteredList);
        return artist;
    }

    private Artist responseBuilder(Artist artist, String description) {
        artist.setDescription(description);
        return pairAlbumWithCover(artist);
    }

    private Artist pairAlbumWithCover(Artist artist) {
        artist.getAlbumList().stream()
                .forEach(album -> {
                    String albumId = album.getId();
                    album.setAlbumArt(coverArtClient.findAlbumCover(albumId));
                });
        return artist;
    }
}
