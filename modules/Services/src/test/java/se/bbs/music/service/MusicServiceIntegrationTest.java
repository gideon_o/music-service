package se.bbs.music.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;
import se.bbs.music.*;
import se.bbs.music.models.Artist;
import se.bbs.music.services.IMusicService;
import se.bbs.music.services.MusicService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestConfiguration.class)
public class MusicServiceIntegrationTest {

    private final String ARTIST_ADELE_MBID = "cc2c9c3c-b7bc-4b8b-84d8-4fbd8779e493";
    private final String ARTIST_ADELE_Name = "Adele";

    @Autowired
    private RestTemplate restTemplate;
    private CoverArtClient coverArtClient;
    private MediaWikiClient mediaWikiClient;
    private MusicbrainzIdClient musicbrainzIdClient;
    private IMusicService musicService;

    @Before
    public void setUp(){
        coverArtClient = new CoverArtClient(restTemplate);
        mediaWikiClient = new MediaWikiClient(restTemplate);
        musicbrainzIdClient = new MusicbrainzIdClient(restTemplate);
        musicService = new MusicService(musicbrainzIdClient, mediaWikiClient, coverArtClient);
    }

    @Test
    public void returnsArtistForAGivenId(){
        Artist artist = musicService.find(ARTIST_ADELE_MBID);
        Assert.assertEquals(ARTIST_ADELE_MBID, artist.getId());
        Assert.assertEquals(ARTIST_ADELE_Name, artist.getName());
    }
}
