package se.bbas.music;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import se.bbs.music.MediaWikiClient;

/**
 * Testing for integration test
 *
 *@author Gideon Oduro <gideon.oduro@gmail.com>
 * */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestConfiguration.class)
public class MediaWikiClientIntegrationTest {

    private final String ARTIST_IDENTIFIER = "Adele";
    private final String NOT_FOUND_IDENTIFIER = "aaaabbbccc";
    private String EXPECTED_DESCRIPTION_TEXT;

    @Autowired
    private RestTemplate restTemplate;
    private MediaWikiClient mediaWikiClient;


    @Before
    public void setUp(){
        mediaWikiClient = new MediaWikiClient(restTemplate);
        EXPECTED_DESCRIPTION_TEXT = getDescriptionText();
    }

    /* Not the best test. Test may fail if changes are med to thw wiki page */
    @Test
    public void returnArtistDescriptionTextForAGivenId(){
        String ACTUAL_DESCRIPTION_STRING = mediaWikiClient.findDescription(ARTIST_IDENTIFIER);
        Assert.assertEquals(EXPECTED_DESCRIPTION_TEXT, ACTUAL_DESCRIPTION_STRING);
    }

    private String getDescriptionText(){
        return "<p><b>Adele Laurie Blue Adkins</b> MBE (<span><span>/<span><span title=\"/ə/ 'a' in 'about'\">ə</span><span title=\"/ˈ/ primary stress follows\">ˈ</span><span title=\"'d' in 'dye'\">d</span><span title=\"/ɛ/ short 'e' in 'bed'\">ɛ</span><span title=\"'l' in 'lie'\">l</span></span>/</span></span>; born 5 May 1988) is an English singer and songwriter. Graduating from the BRIT School for Performing Arts and Technology in 2006, Adele was given a recording contract by XL Recordings after a friend posted her demo on Myspace the same year. In 2007, she received the Brit Awards \"Critics' Choice\" award and won the BBC Sound of 2008 poll. Her debut album, <i>19</i>, was released in 2008 to commercial and critical success. It is certified seven times platinum in the UK, and double platinum in the US. An appearance she made on <i>Saturday Night Live</i> in late 2008 boosted her career in the US. At the 51st Annual Grammy Awards in 2009, Adele received the awards for Best New Artist and Best Female Pop Vocal Performance.</p>\n" +
                "<p>Adele released her second studio album, <i>21</i>, in early 2011. The album was well received critically and surpassed the success of her debut, earning the singer numerous awards in 2012, including a record-tying six Grammy Awards, including Album of the Year; two Brit Awards, including British Album of the Year, and three American Music Awards. The album has been certified 16 times platinum in the UK, and is the fourth best-selling album in the UK of all time. In the US it has held the top position longer than any album since 1985, and is certified Diamond. The album has sold 31 million copies worldwide.</p>\n" +
                "<p>The success of <i>21</i> earned Adele numerous mentions in the <i>Guinness Book of World Records.</i> She is the first woman in the history of the <i>Billboard</i> Hot 100 to have three simultaneous top 10 singles as a lead artist, and the first female artist to simultaneously have two albums in the top five of the <i>Billboard</i> 200 and two singles in the top five of the <i>Billboard</i> Hot 100. <i>21</i> is the longest-running number one album by a female solo artist in the history of the UK and US Album Charts. In 2012, she released \"Skyfall\", which she wrote and recorded for the James Bond film of the same name. The song won an Academy Award, a Grammy Award, and a Golden Globe Award for Best Original Song. After taking a three-year break, Adele released her third studio album, <i>25</i>, on 20 November 2015. The album debuted at number one in most major markets and broke first week sales records in the UK and US. The lead single, \"Hello\", debuted at number one in many countries around the world, and became the first song in the US to sell over one million digital copies within a week of its release.</p>\n" +
                "<p>In 2011 and 2012, <i>Billboard</i> named Adele Artist of the Year. In 2012, she was listed at number five on VH1's 100 Greatest Women in Music, and <i>Time</i> magazine named her one of the most influential people in the world. On 18 December 2015, her sales were estimated at more than 100 million combined singles and albums, making her one of the world's best-selling artists.</p>\n" +
                "<p></p>";
    }


}
