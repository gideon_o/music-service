package se.bbas.music;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import se.bbs.music.CoverArtClient;
import se.bbs.music.models.AlbumArt;
import se.bbs.music.models.AlbumCover;
import se.bbs.music.models.AlbumThumbnail;

import java.util.ArrayList;
import java.util.List;


/**
 * Testing for integration test
 *
 *@author Gideon Oduro <gideon.oduro@gmail.com>
 * */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestConfiguration.class)
public class CoverArtIntegrationTest {

    private final String ADELE_ALBUM_25_MBID = "5537624c-3d2f-4f5c-8099-df916082c85c";
    private final String NOT_FOUND_ALBUM_ID = "abc";

    @Autowired
    private RestTemplate restTemplate;
    private CoverArtClient coverArtClient;
    private AlbumArt EXPECTED_ALBUM_ART;

    @Before
    public void setUp(){
        coverArtClient = new CoverArtClient(restTemplate);
        EXPECTED_ALBUM_ART = generateAlbumArt();
    }

    @Test
    public void returnAlbumArtForAGivenAlbumId(){
        AlbumArt ACTUAL_ALBUM_ART = coverArtClient.findAlbumCover(ADELE_ALBUM_25_MBID);
        Assert.assertEquals(EXPECTED_ALBUM_ART, ACTUAL_ALBUM_ART);
    }

    @Test(expected=HttpClientErrorException.class)
    public void throwExceptionIfAlbumCoverNotFound(){
        coverArtClient.findAlbumCover(NOT_FOUND_ALBUM_ID);
    }

    private AlbumArt generateAlbumArt(){
        AlbumArt albumArt = new AlbumArt();
        albumArt.setAlbumCovers(generateAlbumCovers());
        return albumArt;
    }

    private List<AlbumCover> generateAlbumCovers(){
        List<AlbumCover> albumCovers = new ArrayList<>();
        AlbumCover albumCover = new AlbumCover();

        albumCover.setImage("http://coverartarchive.org/release/944d3546-54d2-4baa-9ad3-3f154a21d99b/12266354356.jpg");
        albumCover.setAlbumThumbnail(generateAlbumThumbnail());
        albumCovers.add(albumCover);
        return albumCovers;
    }

    private AlbumThumbnail generateAlbumThumbnail(){
        AlbumThumbnail albumThumbnail = new AlbumThumbnail();
        albumThumbnail.setLarge("http://coverartarchive.org/release/944d3546-54d2-4baa-9ad3-3f154a21d99b/12266354356-500.jpg");
        albumThumbnail.setSmall("http://coverartarchive.org/release/944d3546-54d2-4baa-9ad3-3f154a21d99b/12266354356-250.jpg");
        return albumThumbnail;
    }
}
