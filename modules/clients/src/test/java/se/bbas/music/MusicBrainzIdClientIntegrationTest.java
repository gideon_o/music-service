package se.bbas.music;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import se.bbs.music.MusicbrainzIdClient;
import se.bbs.music.models.Artist;
import se.bbs.music.models.Relations;
import se.bbs.music.models.ResourceUrl;

import java.util.ArrayList;
import java.util.List;

/**
 * Testing for integration test
 *
 *@author Gideon Oduro <gideon.oduro@gmail.com>
 * */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = TestConfiguration.class)
public class MusicBrainzIdClientIntegrationTest {

    private final String ARTIST_ADELE_MBID = "cc2c9c3c-b7bc-4b8b-84d8-4fbd8779e493";
    private final String RELATION_TYPE_ID = "29651736-fa6d-48e4-aadc-a557c6add1cb";
    private final String ARTIST_NAME = "Adele";

    private Artist EXPECTED_ARTIST;

    @Autowired
    private RestTemplate restTemplate;
    private MusicbrainzIdClient musicbrainzIdClient;

    @Before
    public void setUp(){
        musicbrainzIdClient = new MusicbrainzIdClient(restTemplate);
        EXPECTED_ARTIST = generateArtist();
    }

    @Test
    public void returnArtistForAGivenID(){
        Artist ACTUAL_ARTIST = musicbrainzIdClient.findArtist(ARTIST_ADELE_MBID);
        Assert.assertEquals(EXPECTED_ARTIST.getId(), ACTUAL_ARTIST.getId());
        Assert.assertEquals(EXPECTED_ARTIST.getName(), ACTUAL_ARTIST.getName());
    }

    private Artist generateArtist(){
        Artist artist = new Artist();
        artist.setName(ARTIST_NAME);
        artist.setId(ARTIST_ADELE_MBID);
        artist.setRelationsList(generateRelations());
        return artist;
    }

    private List<Relations> generateRelations(){
        List<Relations> relationsList = new ArrayList<>();
        Relations relations = new Relations();
        relations.setId(RELATION_TYPE_ID);
        relations.setResourceUrl(generateResourceUrl());
        relationsList.add(relations);
        return relationsList;
    }

    private ResourceUrl generateResourceUrl(){
        ResourceUrl resourceUrl = new ResourceUrl();
        resourceUrl.setId("c0b29276-de53-42be-be39-58779c5f7c7c");
        resourceUrl.setUrl("http://en.wikipedia.org/wiki/Adele");
        return resourceUrl;
    }
}
