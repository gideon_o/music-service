package se.bbs.music;

public interface IMediaWikiClient {

    public String findDescription (String resourceUrl);
}
