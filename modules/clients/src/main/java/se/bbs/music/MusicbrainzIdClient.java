package se.bbs.music;

import org.apache.log4j.Logger;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import se.bbs.music.models.Artist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Rest Client to handle API calls to Music Brainz API and
 * parses the value to a Artist data object.
 *
 *@author Gideon Oduro <gideon.oduro@gmail.com>
 * */

@Component
public class MusicbrainzIdClient extends Client implements IMusicbrainzIdClient {
    private Logger log = Logger.getLogger(MusicbrainzIdClient.class);

    @Autowired
    public MusicbrainzIdClient(RestTemplate restTemplate){
        super(restTemplate);
    }

    /**
     * This method is used to call a external API and retrieve information
     * for a given artist.
     * @param id This is the only parameter, it represents a MBID
     * @return Artist This returns a the serialized data object.
     */
    public Artist findArtist(String id) throws HttpClientErrorException{
        Artist artist = null;
        log.debug("Retrieving artist with id: " + id);
        try{
             artist = this.restTemplate.getForObject(
                    uriBuilder(id),
                    Artist.class
            );
        } catch (RestClientException ex){
            log.error("Unable to serialize json to Artist.class for id: " + id, ex);
        }
        return artist;
    }

    private String uriBuilder(String id){
        return "http://musicbrainz.org/ws/2/artist/"+ id +"?fmt=json&inc=url-rels+release-groups&type=album";
    }
}
