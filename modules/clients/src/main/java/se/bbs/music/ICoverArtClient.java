package se.bbs.music;

import se.bbs.music.models.AlbumArt;

public interface ICoverArtClient  {

    public AlbumArt findAlbumCover(String id);
}
