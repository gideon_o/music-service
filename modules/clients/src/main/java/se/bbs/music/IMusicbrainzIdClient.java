package se.bbs.music;

import org.springframework.web.client.HttpClientErrorException;
import se.bbs.music.models.Artist;

public interface IMusicbrainzIdClient {
    public Artist findArtist(String id) throws HttpClientErrorException;
}
