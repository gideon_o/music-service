package se.bbs.music;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

/**
 * Rest Client to handle API calls to Wikipedia Media API and
 * uses JsonNode to navigate to the artist description text
 * in the json node tree structure. This is done due to a
 * nest json object what a mutating field name.
 *
 * Media wiki API
 * https://www.mediawiki.org/wiki/API:Main_page
 *
 *@author Gideon Oduro <gideon.oduro@gmail.com>
 * */

@Component
public class MediaWikiClient extends Client implements IMediaWikiClient{

    Logger log = Logger.getLogger(MusicbrainzIdClient.class);

    private final String JSON_FIELD_QUERY = "query";
    private final String JSON_FIELD_PAGES = "pages";
    private final String JSON_FIELD_EXTRACT = "extract";

    private final int URL_IDENTIFIER = 4;
    private final int SINGLE_IDENTIFIER = 1;



    @Autowired
    public MediaWikiClient(RestTemplate restTemplate) {
        super(restTemplate);
    }

    /**
     * This method is used to call the a external API and retrieve artist description text.
     * @param resourceUrl This is the only parameter, the represents a URL string.
     * @return String This returns the description text.
     */
    public String findDescription(String resourceUrl) throws HttpClientErrorException{
        String identifier = extractIdentifier(resourceUrl);
        log.debug("retrieving description text for artist: " + identifier);
        String descriptionInfo = restTemplate.getForObject(
                    urlBuilder(identifier),
                    String.class);
        return extractDescription(descriptionInfo);
    }

    //TODO fix this ugly method
    private String extractIdentifier(String resourceUrl) {
        String[] urlParts = resourceUrl.split("/");
        if (urlParts.length == SINGLE_IDENTIFIER){
            return resourceUrl;
        }
        return urlParts[URL_IDENTIFIER];
    }

    private String extractDescription(String data){
        JsonNode descriptionNode = null;
        String description = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(data);
            Iterator<Map.Entry<String, JsonNode>> value = root.get(JSON_FIELD_QUERY).get(JSON_FIELD_PAGES).fields();
            Map.Entry<String, JsonNode> innerObject = null;
            while (value.hasNext()){
                innerObject = value.next();
            }
            descriptionNode = innerObject.getValue().get(JSON_FIELD_EXTRACT);
            description = descriptionNode.asText();
        } catch (IOException ex) {
            log.error("Unable to read json data from Media Wiki API", ex);
        } catch (NullPointerException ex){
            log.error("No description text was return by Media wiki API");
        }
        return description;
    }

    private String urlBuilder(String id){
        return "https://en.wikipedia.org/w/api.php?action=query&format=json&prop=extracts&exintro=true&redirects=true&titles=" + id;
    }
}
