package se.bbs.music;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import se.bbs.music.models.AlbumArt;

/**
 * Rest Client to handle API calls to Cover Art API and
 * parses the value to a album art data object.
 *
 * Cover Art API
 * http://coverartarchive.org/
 *
 *@author Gideon Oduro <gideon.oduro@gmail.com>
 * */

@Component
public class CoverArtClient extends Client implements ICoverArtClient{
    private Logger log = Logger.getLogger(CoverArtClient.class);

    @Autowired
    public CoverArtClient(RestTemplate restTemplate) {
        super(restTemplate);
    }

    /**
     * This method is used to call the external API and retrieve album cover
     * for a particular album.
     * @param id This is the only parameter, the represents a Album MBID
     * @return AlbumdArt This returns a the serialized data object.
     */
    public AlbumArt findAlbumCover(String id) throws HttpClientErrorException{
        log.info("Retrieving album cover for id: " + id );
        AlbumArt albumCover = restTemplate.getForObject(
                urlBuilder(id),
                AlbumArt.class);
        return albumCover;
    }

    private String urlBuilder(String id){
        return "http://coverartarchive.org/release-group/" + id;
    }
}
