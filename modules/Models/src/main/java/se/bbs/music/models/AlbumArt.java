package se.bbs.music.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class AlbumArt {

    @JsonProperty("images")
    private List<AlbumCover> albumCovers;

    public AlbumArt() {
    }

    public AlbumArt(List<AlbumCover> albumCovers) {
        this.albumCovers = albumCovers;
    }

    public List<AlbumCover> getAlbumCovers() {
        return albumCovers;
    }

    public void setAlbumCovers(List<AlbumCover> albumCovers) {
        this.albumCovers = albumCovers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AlbumArt)) return false;

        AlbumArt albumArt = (AlbumArt) o;

        return albumCovers.equals(albumArt.albumCovers);

    }

    @Override
    public int hashCode() {
        return albumCovers.hashCode();
    }

    @Override
    public String toString() {
        return "AlbumArt{" +
                "albumCovers=" + albumCovers +
                '}';
    }
}
