package se.bbs.music.models;

public class Album {
    private String id;
    private String title;
    private AlbumArt albumArt;

    public Album() {
    }

    public Album(String id, String title, AlbumArt albumArt) {
        this.id = id;
        this.title = title;
        this.albumArt = albumArt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AlbumArt getAlbumArt() {
        return albumArt;
    }

    public void setAlbumArt(AlbumArt albumArt) {
        this.albumArt = albumArt;
    }
}
