package se.bbs.music.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AlbumCover {

    private String image;

    @JsonProperty("thumbnails")
    private AlbumThumbnail albumThumbnail;

    public AlbumCover() {
    }

    public AlbumCover(String id, String image, AlbumThumbnail albumThumbnail) {
        this.image = image;
        this.albumThumbnail = albumThumbnail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public AlbumThumbnail getAlbumThumbnail() {
        return albumThumbnail;
    }

    public void setAlbumThumbnail(AlbumThumbnail albumThumbnail) {
        this.albumThumbnail = albumThumbnail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AlbumCover)) return false;

        AlbumCover that = (AlbumCover) o;

        if (!image.equals(that.image)) return false;
        return albumThumbnail.equals(that.albumThumbnail);

    }

    @Override
    public int hashCode() {
        int result = image.hashCode();
        result = 31 * result + albumThumbnail.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "AlbumCover{" +
                "image='" + image + '\'' +
                ", albumThumbnail=" + albumThumbnail +
                '}';
    }
}
