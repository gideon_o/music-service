package se.bbs.music.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Artist {

    private String id;
    private String name;
    private String description;
    private List<Album> albumList;
    private List<Relations> relationsList;

    public Artist() {
    }

    public Artist(String id, String name, String description, List<Album> albumList, List<Relations> relationsList) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.albumList = albumList;
        this.relationsList = relationsList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("Albums")
    public List<Album> getAlbumList() {
        return albumList;
    }

    @JsonProperty("release-groups")
    public void setAlbumList(List<Album> albumList) {
        this.albumList = albumList;
    }

    @JsonIgnore
    public List<Relations> getRelationsList() {
        return relationsList;
    }

    @JsonProperty("relations")
    public void setRelationsList(List<Relations> relationsList) {
        this.relationsList = relationsList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Artist)) return false;

        Artist artist = (Artist) o;

        if (!id.equals(artist.id)) return false;
        if (!name.equals(artist.name)) return false;
        if (!description.equals(artist.description)) return false;
        if (!albumList.equals(artist.albumList)) return false;
        return relationsList.equals(artist.relationsList);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + albumList.hashCode();
        result = 31 * result + relationsList.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Artist{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", albumList=" + albumList +
                ", relationsList=" + relationsList +
                '}';
    }
}
