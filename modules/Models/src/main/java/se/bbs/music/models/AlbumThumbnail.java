package se.bbs.music.models;

public class AlbumThumbnail {

    private String large;
    private String small;

    public AlbumThumbnail() {
    }

    public AlbumThumbnail(String large, String small) {
        this.large = large;
        this.small = small;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AlbumThumbnail)) return false;

        AlbumThumbnail that = (AlbumThumbnail) o;

        if (!large.equals(that.large)) return false;
        return small.equals(that.small);

    }

    @Override
    public int hashCode() {
        int result = large.hashCode();
        result = 31 * result + small.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "AlbumThumbnail{" +
                "large='" + large + '\'' +
                ", small='" + small + '\'' +
                '}';
    }
}
