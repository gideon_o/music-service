package se.bbs.music.models;


import com.fasterxml.jackson.annotation.JsonProperty;

public class Relations {

    @JsonProperty("type-id")
    private String id;
    private String type;

    @JsonProperty("url")
    private ResourceUrl resourceUrl;

    public Relations() {
    }

    public Relations(String id, String type, ResourceUrl resourceUrl) {
        this.id = id;
        this.type = type;
        this.resourceUrl = resourceUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ResourceUrl getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(ResourceUrl resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Relations)) return false;

        Relations relations = (Relations) o;

        if (!id.equals(relations.id)) return false;
        if (!type.equals(relations.type)) return false;
        return resourceUrl.equals(relations.resourceUrl);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + resourceUrl.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Relations{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", resourceUrl=" + resourceUrl +
                '}';
    }
}
