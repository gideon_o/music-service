package se.bbs.music.models;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ResourceUrl {

    private String id;
    @JsonProperty("resource")
    private String url;

    public ResourceUrl() {
    }

    public ResourceUrl(String id, String url) {
        this.id = id;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ResourceUrl)) return false;

        ResourceUrl that = (ResourceUrl) o;

        if (!id.equals(that.id)) return false;
        return url.equals(that.url);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + url.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ResourceUrl{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
