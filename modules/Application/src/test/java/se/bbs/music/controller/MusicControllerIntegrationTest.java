package se.bbs.music.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import se.bbs.music.Application;
import se.bbs.music.controller.ApiConstants.RequestMappingConstants;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration
@ContextConfiguration(classes = Application.class)
@WebAppConfiguration
public class MusicControllerIntegrationTest {

    private final String JSON_RESPONSE_FIELD_NAME_ID = "$.id";
    private final String ARTIST_NOT_FOUND_MBID = "aaabbbcc";
    private final String ARTIST_ADELE_MBID = "cc2c9c3c-b7bc-4b8b-84d8-4fbd8779e493";
    private final String JSON_RESPONSE_FIELD_NAME = "$.name";
    private final String JSON_RESPONSE_FIELD_NAME_VALUE = "Adele";
    private final String JSON_RESPONSE_FIELD_MESSAGE = "$.message";
    private final String JSON_RESPONSE_FIELD_MESSAGE_VALUE = "No artist found, please check the provided MBID: ";

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext webApplicationContext;

    @Before
    public void setUp(){
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void returnArtistForAValidId() throws Exception{
        mockMvc.perform(
                get(RequestMappingConstants.MUSIC_SERVICE + "/" + ARTIST_ADELE_MBID)
        ).andExpect(status().isOk())
                .andExpect(
                        jsonPath(JSON_RESPONSE_FIELD_NAME_ID)
                                .value(ARTIST_ADELE_MBID))
                .andExpect(
                        jsonPath(JSON_RESPONSE_FIELD_NAME)
                                .value(JSON_RESPONSE_FIELD_NAME_VALUE));
    }

    @Test
    public void returnArtistNotFoundErrorIfArtistNotFound() throws Exception{
        mockMvc.perform(
                get(RequestMappingConstants.MUSIC_SERVICE + "/" + ARTIST_NOT_FOUND_MBID)
        ).andExpect(status().is4xxClientError())
                .andExpect(
                        jsonPath(JSON_RESPONSE_FIELD_MESSAGE)
                                .value(JSON_RESPONSE_FIELD_MESSAGE_VALUE + ARTIST_NOT_FOUND_MBID));
    }
}
