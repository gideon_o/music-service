package se.bbs.music.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.bbs.music.controller.ApiConstants.RequestMappingConstants;
import se.bbs.music.exception.ArtistNotFoundException;
import se.bbs.music.exception.MusicServiceExceptionHandler;
import se.bbs.music.models.Artist;
import se.bbs.music.services.IMusicService;

/**
 * The Music Controller is the only endpoint when using the Beach Boys sweden
 * Music service. This API requires MBID associated with an artist or a band.
 * MBID:s can easily be found on the following page: http://www.musicbrainz.org
 *
 * Example request when running on a local environment, for the artist Adele:
 * http://localhost:8085/1.0/api/music/cc2c9c3c-b7bc-4b8b-84d8-4fbd8779e493
 *
 *@author Gideon Oduro <gideon.oduro@gmail.com>
 * */

@RestController
@RequestMapping(value = RequestMappingConstants.MUSIC_SERVICE)
public class MusicController {

    @Autowired
    IMusicService musicService;

    @RequestMapping(value={"/{artistId}"}, method = RequestMethod.GET)
    public Artist findArtist(@PathVariable(value="artistId") String id) throws ArtistNotFoundException {
        Artist artist = musicService.find(id);
        if (artist == null)
            throw new ArtistNotFoundException("No artist found, please check the provided MBID: " + id);
        return musicService.find(id);
    }
}
