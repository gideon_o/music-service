package se.bbs.music.exception;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import se.bbs.music.models.ErrorResponse;

@ControllerAdvice
public class MusicServiceExceptionHandler extends Exception {

    private static final Logger log = Logger.getLogger(MusicServiceExceptionHandler.class);

    @ExceptionHandler(ArtistNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorResponse ArtistNotFoundException(Exception exception){
        return new ErrorResponse(exception.getMessage());
    }

    @ExceptionHandler(MissingPathVariableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse handleBadRequestException(Exception exception){
        log.debug("Bad request: ", exception);
        return new ErrorResponse("Please validate that your using a correct formatted url");
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorResponse handleError(Exception exception){
        log.error("Unhandled exception intercepted: ", exception);
        return new ErrorResponse("Internal error, we apologies for the inconvenient");
    }
}
