package se.bbs.music.controller.ApiConstants;

public class RequestMappingConstants {

    public static final String VERSION_BASE_MAPPING = "/1.0";
    public static final String MUSIC_SERVICE = VERSION_BASE_MAPPING + "/api/music";
}
