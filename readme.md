# Music Server API #


## Setup in local environment

**Requirements**
- Apache Maven 3
- Java JDK 1.8

1. Start by cloning the project 

2. Navigate to: Music-service folder.  


3. Use maven to perform a:
    ```
    mvn clean install
    ```
4. Navigate to: /modules/Application folder.  


5. Run with Maven Spring Boot plugin  
    ```
    mvn spring-boot:run
    ``` 

The API is running on localhost:8085. See below for API specifications.

## API specification 

[Postman](https://www.getpostman.com/), a REST client for performing requests.

### Request data for a specific artist.

**HTTP method:** `GET`

**Path:** `/1.0/api/music/<MBID>`

**example url:** `localhost:8085/1.0/api/music/cc2c9c3c-b7bc-4b8b-84d8-4fbd8779e493`

**Example response:**

HTTP status code: 200 OK

Response body: `{
  "id": "cc2c9c3c-b7bc-4b8b-84d8-4fbd8779e493",
  "name": "Adele",
  "description": "<p><b>Adele Laurie Blue Adkins</b> ...",
  "Albums": [
    {
      "id": "1c779f87-f311-485d-b0f4-99cc23fcfdfb",
      "title": "88 (Mixed By Mick Boogie)",
      "albumArt": {
        "images": [
          {
            "image": "http://coverartarchive.org/release/81c7d218-35ab-42f0-81b0-bfc85d3d4cb8/12726004945.jpg",
            "thumbnails": {
              "large": "http://coverartarchive.org/release/81c7d218-35ab-42f0-81b0-bfc85d3d4cb8/12726004945-500.jpg",
              "small": "http://coverartarchive.org/release/81c7d218-35ab-42f0-81b0-bfc85d3d4cb8/12726004945-250.jpg"
            }
          }
        ]
      }
    }, 
    ...
  ]
}`